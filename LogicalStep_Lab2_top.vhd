library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity LogicalStep_Lab2_top is port (
   clkin_50			: in	std_logic;
	pb				: in	std_logic_vector(3 downto 0);
 	sw   			: in  std_logic_vector(7 downto 0); -- The switch inputs
   leds				: out std_logic_vector(7 downto 0); -- for displaying the switch content
   seg7_data 		: out std_logic_vector(6 downto 0); -- 7-bit outputs to a 7-segment
	seg7_char1  	: out	std_logic;				    		-- seg7 digit1 selector
	seg7_char2  	: out	std_logic				    		-- seg7 digit2 selector
	
); 
end LogicalStep_Lab2_top;

architecture SimpleCircuit of LogicalStep_Lab2_top is
--
-- Components Used ---
------------------------------------------------------------------- 
  component SevenSegment port (
   hex   		:  in  std_logic_vector(3 downto 0);   -- The 4 bit data to be displayed
   sevenseg 	:  out std_logic_vector(6 downto 0)    -- 7-bit outputs to a 7-segment
   ); 
   end component;
	
	component segment7_mux port(
		clk: in std_logic := '0';
		DIN2: in std_logic_vector(6 downto 0);
		DIN1: in std_logic_vector(6 downto 0);
		DOUT: out std_logic_vector(6 downto 0);
		DIG2: out std_logic;
		DIG1: out std_logic
	);
	end component;
	
	component section1_mux port (
		hexAB 		: in  std_logic_vector(7 downto 0);
		hex_sum		: in  std_logic_vector(7 downto 0);
		push3		: in  std_logic := '1';
		hex_out 	: out std_logic_vector(7 downto 0)
		);
	end component;
	
-- Create any signals, or temporary variables to be used
--
--  std_logic_vector is a signal which can be used for logic operations such as OR, AND, NOT, XOR
--
	signal seg7_A		: std_logic_vector(6 downto 0);
	signal seg7_B		: std_logic_vector(6 downto 0);
	signal hex_A		: std_logic_vector(3 downto 0);
	signal hex_B		: std_logic_vector(7 downto 4);
	signal hex_concat   : std_logic_vector(7 downto 0);
	signal sum 			: std_logic_vector(7 downto 0);
	signal pb3 			: std_logic_vector(3 downto 0);
	signal hexOUT		: std_logic_vector(7 downto 0);
	signal hexOUT_A		: std_logic_vector(3 downto 0);
	signal hexOUT_B 	: std_logic_vector(3 downto 0);

-- Here the circuit begins

begin

	hex_A <= sw(3 downto 0);
	hex_B <= sw(7 downto 4);
	pb3 <= pb(3);
	hex_concat <= hex_A & hex_B;
	
-- COMPONENT HOOKUP
--
-- generate the seven segment coding
	INST1: SevenSegment port map(hexOUT_A, seg7_A);
	INST2: SevenSegment port map(hexOUT_B, seg7_B);
	INST3: segment7_mux port map(clk => clkin_50, DIN2 => seg7_A, DIN1 => seg7_B, DOUT => seg7_data, DIG2 => seg7_char2, DIG1 => seg7_char1);
	INST4: section1_mux port map(hexAB => hex_concat, hex_sum => sum, push3 => pb3, hex_out => hexOUT(3 downto 0 => hexOUT_A, 7 downto 4 => hexOUT_B));
	
 
end SimpleCircuit;

