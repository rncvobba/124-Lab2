library ieee;
use ieee.std_logic_1164.all;


entity section1_mux is 
port (
	hexAB 		: in  std_logic_vector(7 downto 0);
	hex_sum		: in  std_logic_vector(7 downto 0);
	push3		: in  std_logic := '1';
	hex_out 	: out std_logic_vector(7 downto 0)
);

end entity section1_mux

architecture sec1_logic of hexAB_cat is 

begin 
	hex_out <= hexAB when push3 = '1' else hex_sum;

end sec1_logic;